<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18-1-16
 * Time: 下午2:18
 */

class DeviceAccount
{
    // 用户名 => string
    private $id;

    // 用户密码 => string
    private $pwd;

    // 用户token => string
    private $token;

    // 用户登录状态 => int
    private $state;

    // 腾讯云互动直播签名 => string
    private $userSig;

    // 注册时间 => int
    private $registerTime;

    // 登录时间 => int
    private $loginTime;

    // 注销时间 => int
    private $logoutTime;

    // 最近一次请求时间 => int
    private $lastRequestTime;

    // 设备类型 => int
    private $deviceType;

    // 序列号 => string
    private $serialNumber;

    // 生产厂家 => string
    private $manufacturer;

    // 生产日期 => int
    private $manufactureDate;

    // 失效日期 => int
    private $expiryDate;


    /* 功能：通过用户名获取用户的个人账号信息
      * 说明：用户名通过Account对象成员获取，查询到的个人账号信息直接存储在
      *        Account对象成员中；成功返回0，error_msg为空；失败则返回错误码，
      *        设置错误信息error_msg。
      */
    public function getAccountRecordByUserID(&$error_msg)
    {

    }

    /* 功能：通过用户Token获取用户的个人账号信息
     * 说明：用户Token通过Account对象成员获取，查询到的个人账号信息直接存储在
     *        Account对象成员中；成功返回0，error_msg为空；失败则返回错误码，
     *        设置错误信息error_msg。
     */
    public function getAccountRecordByToken(&$error_msg)
    {

    }

    /* 功能：用户Token转换用户名
     * 说明：用户Token通过Account对象成员token获取，查询到的用户名直接存储在
     *        Account对象成员uid中；成功返回0，error_msg为空；失败则返回错误码，
     *        设置错误信息error_msg。
     */
    public function getAccountUidByToken(&$error_msg)
    {

    }

    public function genUserSig($sdkappid, $private_key_path)
    {

    }

    /* 功能：用户密码正确性验证
     * 说明：用户输入密码pwd和DB中的pwd解密比对；DB中的密码使用base64加密存储
     */
    public function authentication($pwd, &$error_msg)
    {

    }

    /* 功能：生成token
     * 说明：生成方式：用户名+登录时间再base64加密。用户名是唯一，因此用户token一定唯一
     *        成功，返回token；失败返回空。开发人员可以自定义，只要保证token唯一即可
     */
    public function genToken()
    {

    }

    /* 功能：设备端注册
    * 说明：成功返回ERR_SUCCESS，error_msg为空；失败则返回错误码，设置错误信息error_msg。
    */
    public function register(&$error_msg)
    {

    }

    /* 功能：用户登录
     * 说明：成功返回ERR_SUCCESS，error_msg为空；失败则返回错误码，设置错误信息error_msg。
     *          存储token，有效期内免登陆；更新时间
     */
    public function login(&$error_msg)
    {

    }

    /* 功能：注销登录
    * 说明：成功返回ERR_SUCCESS，error_msg为空；失败则返回错误码，设置错误信息error_msg。
    *        删除token
    */
    public function logout(&$error_msg)
    {

    }

    /* 功能：删除已注册设备
    */
    static public function deleteDeviceAccount($id)
    {

    }

    /* 功能：修改已注册设备信息
    */
    static public function modifyUserAccountInfo($id, $data)
    {

    }

    /* 功能：查看设备的注册信息及当前状态
   */
    static public function getList()
    {

    }

    static public function getDeviceAccountRecordByUserID($id)
    {

    }
}
