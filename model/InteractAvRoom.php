<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18-1-16
 * Time: 下午2:21
 */

class InteractAvRoom
{
    // 用户名 => string
    private $uid;

    //房间ID => int
    private $avRoomId = -1;

    //上下麦状态 => string; 状态:on-上麦，off-下麦
    private $status = '';

    //心跳时间 => int
    private $modifyTime = 0;

    //成员角色 => int；0-观众；1-主播；2-上麦成员
    private $role = 0;

    /* 功能：成员进入房间
     * 说明：如果成员已经存在，覆盖。成功：true, 出错：false
     */
    public function enterRoom()
    {

    }

    /* 功能：成员退出房间
     * 说明：成功：true, 出错：false
     */
    public function exitRoom()
    {

    }

    /* 功能：清空房间成员
     * 说明：用于直播结束清空房间成员；成功：true, 出错：false
     */
    static public function ClearRoomByRoomNum($avRoomId)
    {

    }

    /* 功能：获取房间成员
     *      成功返回房间成员信息，失败返回空
     */
    public static function getList($roomnum, $appid = 0)
    {

    }

    /* 功能：获取房间成员总数
    * 说明：APP（appid）的房间（roomnum）的成员总数；
    *      成功返回房间成员总数，失败返回-1
    */
    public static function getCount($roomnum, $appid = 0)
    {

    }

    /* 功能：更新成员心跳时间
     * 说明：更新用户（uid）的心跳时间（time）；role角色
     *      成功返回true，失败返回false
     */
    static public function updateLastUpdateTimeByUid($uid, $role, $time, $video_type)
    {

    }

    /* 功能：删除僵尸成员
     * 说明：由定时清理程序调用。删除心跳超过定时（inactiveSeconds）时间的成员
     *      成功返回true，失败返回false
     */
    public static function deleteDeathRoomMember($inactiveSeconds, $role = 0)
    {

    }
}